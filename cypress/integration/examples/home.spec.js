describe('login', function () {
    it('should log in', function () {

        cy.visit('https://togo-digital.ucasenvironments.com/account/login');

        cy.get('#Email').type('bbtsuperuser@ucas.ac.uk')
        cy.get('#Password').type('F1ndMyBugz!')
        cy.get('#LoginSubmit').click()
        cy.get('.tabs__tab').contains('Postgraduates').click()
        cy.get('.button').contains('Go to Apply').click()
        cy.xpath('.//a[@class="button button--small button--primary spinner"]').click()
        cy.get('#SearchText').type('Edge Hill')
        cy.get('#SearchSubmit').click()
        cy.get('[for="Destination_Postgraduate"]').click()
        cy.get('[for="Scheme_UCAS_Postgraduate_checkbox"]').click()
        cy.get('.course-title').contains('Advanced Computer Networking').click()
        cy.get('.impact').contains('Ormskirk Campus').click()
        cy.xpath('//div/a[contains(text(),"Apply")]').click()
        
    });
});